﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Duplicate_Encoder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            /*
            Цель этого упражнения состоит в том,
                чтобы преобразовать строку в новую строку,
                где каждый символ в новой строке "(" если этот символ появляется только один раз в исходной строке,
                или ")" если этот символ появляется более одного раза в исходной строке.
                Игнорируйте заглавные буквы при определении того, является ли символ дубликатом.

                Примеры
            "din"      =>  "((("
            "recede"   =>  "()()()"
            "Success"  =>  ")())())"
            "(( @"     =>  "))(("

            Примечания
                Сообщения с утверждениями могут быть неясны относительно того,
                что они отображают на некоторых языках.
                Если вы читаете "...It Should encode XXX", в "XXX" это ожидаемый результат, а не входные данные!

            */
        }
    }
    public class Kata
    {
        public static string DuplicateEncode(string word)
        {

            word = word.ToLower();
            string Itog = "";
            foreach (var item in word)
            {
                if (NovayaFunq(word, item) > 1)
                {
                    Itog += ")";
                }
                else
                {
                    Itog += "(";
                }
            }
            return Itog;
        } 
        public static int NovayaFunq(string word, char a)
        {
            int aFind = 0;
            foreach (var i in word)
            {
                if (i == a)
                {
                    aFind++;
                }
            }

            return aFind;
        }
    }

    public class KataLinq
    {
        public static string DuplicateEncode(string word)
        {
            return string.Join("", word.ToLower().Select(w => word.ToLower().Count(c => w == c) > 1 ? ')' : '('));

        }
    }

}
